# diskinfo

A colorful replacement for `lsblk`, `gdisk -l`, `blkid`, `df`, and `mount` that
gives a complete overview of disks attached to your system.

![](docs/diskinfo.png)

## Setup

This project is currently distributed as a ruby gem. On a system with ruby
installed, run the following command:

```sh
gem install aspen-diskinfo
```

## Usage

Simply run the following, as any user, to show information on your disks:

```sh
diskinfo
```

This tool doesn't take any options.
