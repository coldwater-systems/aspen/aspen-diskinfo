# frozen_string_literal: true

require_relative 'lib/aspen/diskinfo/version'

Gem::Specification.new do |spec|
  spec.name        = 'aspen-diskinfo'
  spec.version     = Aspen::Diskinfo::VERSION
  spec.summary     = 'A colorful overview in your terminal of disks attached to your system.'
  spec.description = <<~DESC
    A colorful replacement for `lsblk`, `gdisk -l`, `blkid`, `df`, and `mount`
    that gives a complete overview of disks attached to your system.
  DESC
  spec.author      = 'Alex Gittemeier'
  spec.email       = 'me@a.lexg.dev'
  spec.license     = 'GPL-3.0-only'

  spec.metadata['homepage_uri']      = 'https://gitlab.com/coldwater-systems/aspen/aspen-diskinfo/-/tree/main'
  spec.metadata['source_code_uri']   = 'https://gitlab.com/coldwater-systems/aspen/aspen-diskinfo/-/tree/stable'
  spec.metadata['documentation_uri'] = 'https://www.rubydoc.info/gems/aspen-diskinfo/'
  spec.metadata['bug_tracker_uri']   = 'https://gitlab.com/coldwater-systems/aspen/aspen-diskinfo/-/issues'
  spec.metadata['changelog_uri']     = 'https://gitlab.com/coldwater-systems/aspen/aspen-diskinfo/-/commits/stable'
  spec.metadata['rubygems_mfa_required'] = 'true'
  spec.homepage = spec.metadata['homepage_uri']

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) {|f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 2.7.0'
  spec.add_dependency 'colorize', '~> 0.8.1'

  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rubocop', '~> 1.36'
  spec.add_development_dependency 'rubocop-rake', '~> 0.6'
  spec.add_development_dependency 'rubocop-rspec', '~> 2.13'
  spec.add_development_dependency 'yard', '~> 0.9'
end
