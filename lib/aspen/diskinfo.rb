# frozen_string_literal: true

require 'colorize'
require_relative 'diskinfo/version'
require_relative 'diskinfo/disk'
require_relative 'diskinfo/partition'

module Aspen
  # A colorful overview in your terminal of disks attached to your system.
  module Diskinfo
    def self.humanize_size(size)
      approx_size =
        if size >= 1_000_000_000_000
          format('%5.1f TB', size.fdiv(1_000_000_000_000))
        elsif size >= 1_000_000_000
          format('%5.1f GB', size.fdiv(1_000_000_000))
        elsif size >= 1_000_000
          format('%5.1f MB', size.fdiv(1_000_000))
        elsif size >= 1_000
          format('%5.1f kB', size.fdiv(1_000))
        else
          return "#{size} Bytes".red
        end
      "#{approx_size.red} (#{size} Bytes)"
    end

    def self.humanize_block_size(size, sector_size)
      return nil if !size

      if size % sector_size == 0
        humanize_size(size) + " (exactly #{size / sector_size} sectors)".light_black
      else
        humanize_size(size) + " (#{size / sector_size} sectors + #{size % sector_size} Bytes)".light_black
      end
    end

    def self.print(disks = Disk.all, stdout: $stdout)
      disks.each do |disk|
        partitions = Partition.all_for(disk.device_path)

        vertical_bar = '│'
        vertical_bar = ' ' if partitions.empty?

        stdout.puts "#{disk.device_path.yellow} (#{disk.flags.join(', ')})"
        stdout.print "#{vertical_bar} Serial: #{disk.serial}   Model: #{disk.model}"
        stdout.print " Rev #{disk.rev}" if disk.rev && !disk.rev.strip.empty?
        stdout.puts
        stdout.puts "#{vertical_bar} Size: #{humanize_block_size(disk.size, disk.physical_sector_size)}"

        partitions.each do |part|
          stdout.puts vertical_bar.to_s

          if part.eql?(partitions.last)
            vertical_bar = ' '
            stdout.print '└─ '
          else
            stdout.print '├─ '
          end
          stdout.puts "#{part.device_path.yellow} #{humanize_block_size(part.size, disk.physical_sector_size)}"
          next if !part.filesystem_type

          stdout.puts "#{vertical_bar}    " \
            "#{part.filesystem_type.green} #{part.label&.inspect} #{"UUID=#{part.uuid}".light_black}"
          next if !part.mount_point

          if part.filesystem_type == 'swap'
            stdout.puts "#{vertical_bar}    #{'[swap on]'.cyan}"
            next
          end

          total_space_str = humanize_block_size(part.filesystem_size, disk.physical_sector_size)
          used_pct_str = part.filesystem_used_percent || ''
          used_str = humanize_block_size(part.filesystem_used, disk.physical_sector_size)
          available_str = humanize_block_size(part.filesystem_available, disk.physical_sector_size)
          stdout.puts "#{vertical_bar}    Mounted at : #{part.mount_point.cyan}"
          stdout.puts "#{vertical_bar}    Total Space: #{total_space_str}"
          stdout.puts "#{vertical_bar}    Used #{used_pct_str.rjust(6)}: #{used_str}"
          stdout.puts "#{vertical_bar}    Available  : #{available_str}"
        end
        stdout.puts
      end
    end
  end
end
