# frozen_string_literal: true

require 'json'

module Aspen
  module Diskinfo
    # A data structure representing info about a disk as reported by lsblk(8)
    class Disk
      FIELDS = {
        'path' => :device_path,
        'model' => :model,
        'serial' => :serial,
        'rev' => :rev,
        'size' => :size,
        'pttype' => :partition_table_type,
        'hotplug' => :hot_pluggable,
        'rm' => :removable,
        'ro' => :read_only,
        'rota' => :rotational,
        'phy-sec' => :physical_sector_size,
      }.freeze

      def self.all
        IO.popen(%W[lsblk --output=#{FIELDS.keys.join(',')} --nodeps --json --bytes]) do |lsblk|
          JSON.parse(lsblk.read)['blockdevices'].map do |disk_json|
            new(**disk_json.transform_keys(&FIELDS.method(:fetch)))
          end
        end
      end

      def initialize(**fields)
        if FIELDS.values.sort != fields.keys.sort
          raise ArgumentError, "Expected #{FIELDS.values} but got #{fields.keys}"
        end

        fields.each do |field, value|
          instance_variable_set("@#{field}", value)
        end
      end

      attr_reader(*FIELDS.values)
      alias hot_pluggable? hot_pluggable
      alias removable? removable
      alias read_only? read_only
      alias rotational? rotational

      def flags
        human_flags = []
        human_flags << 'solid-state' if !rotational?
        human_flags << 'hot-pluggable' if hot_pluggable?
        human_flags << (removable? ? 'removable' : 'internal')
        human_flags << 'read-only' if read_only?
        human_flags << if physical_sector_size % 1024 == 0
                         "#{physical_sector_size / 1024}K sectors"
                       else
                         "#{physical_sector_size}B sectors"
                       end
        human_flags << partition_table_type.upcase if partition_table_type
      end
    end
  end
end
