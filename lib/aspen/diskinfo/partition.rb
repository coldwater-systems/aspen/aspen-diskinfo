# frozen_string_literal: true

require 'json'

module Aspen
  module Diskinfo
    # A data structure representing info about a partition as reported by lsblk(8)
    class Partition
      FIELDS = {
        'path' => :device_path,
        'uuid' => :uuid,
        'label' => :label,
        'size' => :size,
        'fstype' => :filesystem_type,
        'fssize' => :filesystem_size,
        'fsused' => :filesystem_used,
        'fsavail' => :filesystem_available,
        'fsuse%' => :filesystem_used_percent,
        'mountpoint' => :mount_point,
      }.freeze
      def self.all_for(device_path)
        IO.popen(%W[lsblk --output=#{FIELDS.keys.join(',')} --json --bytes #{device_path}]) do |lsblk|
          partitions_json = JSON.parse(lsblk.read)['blockdevices']
          partitions_json.reject! {|partition_json| partition_json['path'] == device_path }
          partitions_json.map! do |partition_json|
            partition_json['fssize'] &&= Integer(partition_json['fssize'])
            partition_json['fsused'] &&= Integer(partition_json['fsused'])
            partition_json['fsavail'] &&= Integer(partition_json['fsavail'])

            new(**partition_json.transform_keys(&FIELDS.method(:fetch)))
          end
        end
      end

      def initialize(**fields)
        if FIELDS.values.sort != fields.keys.sort
          raise ArgumentError, "Expected #{FIELDS.values} but got #{fields.keys}"
        end

        fields.each do |field, value|
          instance_variable_set("@#{field}", value)
        end
      end

      attr_reader(*FIELDS.values)
    end
  end
end
