# frozen_string_literal: true

module Aspen
  module Diskinfo
    VERSION = '1.0.2.pre'
  end
end
